import tensorflow as tf
from tensorflow import layers

# He initializer for the layers with ReLU activation function:
he_init = tf.contrib.layers.variance_scaling_initializer(factor=2.0, mode='FAN_IN', uniform=False)
b_init = tf.zeros_initializer()


class Detector(object):

    def __init__(self, input_data, is_training, n_classes=2, padding='same', name='Detector'):
        """
        Class for the Detector architecture. It receives the squared patch as input, outputs the predicted class.
        :param input_data: (tensor) incoming tensor with dimension [None, patch_size, patch_size, 1]
        :param is_training: (tf.placeholder(dtype=tf.bool) or bool) variable to define training or test mode; it is
                        needed for the behaviour of dropout, batch normalization, ecc. (which behave differently
                        at train and test time)
        :param n_classes: (int) number of classes for the network output. Default = 2 (MC is present or not)
        :param padding: (string) padding, can be 'same' or 'valid'. Default = 'same'
        :param name: (string) name scope for the model

        - - - - - - - - - - - - - - - -
        Notice that:
          - the output is linear. In order to compute losses such as categorical cross-entropy you need to add a
            probabilistic interpretation to the output, e.g. adding a softmax (see example below).
        - - - - - - - - - - - - - - - -

        Example of usage:

            # build the entire model:
            model = Detector(input_data, is_training).build()
            output = model.get_prediction()
            soft_output = tf.nn.softmax(output)

            predicted_class = tf.nn.argmax(output)

            loss = weighted_cross_entropy(soft_output, y_true)


        """
        # check for compatible input dimensions

        self.input_data = input_data
        self.n_classes = n_classes
        self.is_training = is_training
        self.padding = padding
        self.name = name

    def build(self):
        """
        Build the model.
        """

        with tf.variable_scope(self.name):

            cpb_1 = self._conv_pool_brick(self.input_data, n_filters=16, name='conv_pool_brick_1')
            cpb_2 = self._conv_pool_brick(cpb_1, n_filters=32, name='conv_pool_brick_2')

            cb_1 = self._conv_brick(cpb_2, n_filters=16, name='conv_brick_1')
            cb_2 = self._conv_brick(cb_1, n_filters=16, name='conv_brick_2')
            cb_3 = self._conv_brick(cb_2, n_filters=16, name='conv_brick_3')
            cb_4 = self._conv_brick(cb_3, n_filters=32, name='conv_brick_4')

            cb_4 = tf.layers.flatten(cb_4)
            fc1 = layers.dense(cb_4, units=64, name='fc1')
            fc2 = layers.dense(fc1, units=self.n_classes, name='fc2')

            self.output = fc2

        return self

    def _conv_pool_brick(self, incoming, n_filters, name):
        """ Convolution + max pooling + batch normalization """
        with tf.variable_scope(name):
            conv1 = layers.conv2d(incoming, filters=n_filters, kernel_size=3, strides=1, padding=self.padding,
                                  kernel_initializer=he_init, bias_initializer=b_init)
            conv1_pool = layers.max_pooling2d(conv1, pool_size=2, strides=1)
            conv1_bn = layers.batch_normalization(conv1_pool, training=self.is_training)
            conv1_act = tf.nn.relu(conv1_bn)
        return conv1_act

    def _conv_brick(self, incoming, n_filters, name):
        """ Convolution + batch normalization """
        with tf.variable_scope(name):
            conv1 = layers.conv2d(incoming, filters=n_filters, kernel_size=3, strides=1, padding=self.padding,
                                  kernel_initializer=he_init, bias_initializer=b_init)
            conv1_bn = layers.batch_normalization(conv1, training=self.is_training)
            conv1_act = tf.nn.relu(conv1_bn)
        return conv1_act

    def get_prediction(self):
        return self.output

