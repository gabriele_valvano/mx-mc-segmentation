"""
Configuration file
"""
import tensorflow as tf

RUN_ID = 'MXMCSegmentation'


def define_flags():
    FLAGS = tf.flags.FLAGS
    tf.flags.DEFINE_string('RUN_ID', RUN_ID, "")

    # ____________________________________________________ #
    # ========== ARCHITECTURE HYPER-PARAMETERS ========== #

    # Learning rate:
    lr = 1e-3
    tf.flags.DEFINE_float('lr', lr, "learning rate")

    # batch size
    tf.flags.DEFINE_integer('b_size', 256, "batch size")

    return FLAGS
