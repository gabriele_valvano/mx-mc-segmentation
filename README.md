# Convolutional Neural Networks for the Segmentation of Microcalcification in Mammography Imaging

**Tensorflow implementation of Segmentator and Detector neural networks**

This repository contains a basic implementation of the network for the Segmentator and the Detector CNNs.
For further details refer to the paper:

> G. Valvano, G. Santini, N. Martini, A. Ripoli, C. Iacconi, D. Chiappino and D. Della Latta. *Convolutional Neural Networks 
for the segmentation of microcalcification in Mammography Imaging*. Journal of Healthcare Engineering 2019 (2019).

----------------------------------
**Data:**

For our experiments, we used 283 mammography images with a resolution of 0.05 mm. Among these images, there are both natively 
digital mammograms and digitized images. Every image is associated to the corresponding manual segmentation mask realized by a 
breast imaging radiologist. We randomly chose 231 mammograms and the annotated labels to build the training set while 25 mammographic
images were used to validate intermediate results and compare different networks architectures. +e remaining 27 images were taken 
apart to build the test set and measure the final performances.

We chose a patch-based approach to process the input mammograms assuming the local information sufficient to
classify such small and circumscribed regions.

We contemplated 4 possible patch classes:
 - **Class C1**: patches whose central pixel belongs to a microcalcification
 - **Class C2**: patches with MCs close to the center but with the central pixel not belonging to a calcification
 - **Class C3**: cases where a calcification resides inside the patch but is located peripherally, and the central pixel does not 
   belong to a MC
 - **Class C4**: cases where no MC is present inside the patch

----------------------------------
**Results:**

Figure 1 shows an example of the breast segmentation process on a zoomed region of the mammogram.

![image](results/images/figure1.png)

Results in Table 2 show the test error rate obtained for each of the 4 classes and the overall test accuracy. 
In particular, the table presents all values obtained using a *valid* and *same* convolutions for both Detector and Segmentator.

![table](results/images/table2.png)


---------------------

For problems, bugs, etc. please contact me at the following email addresses:

  *gabriele.valvano@imtlucca.it* 

Enjoy the code! :)


**Gabriele**